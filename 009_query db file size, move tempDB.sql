USE TempDB
GO
EXEC sp_helpfile
GO

/*
USE master ;
GO
ALTER DATABASE tempdb
MODIFY FILE
(NAME = tempdev, FILENAME = 'E:\sqldata\Tempdb.mdf');
GO
ALTER DATABASE tempdb
MODIFY FILE
(NAME = templog, FILENAME = 'E:\sqldata\Tempdb.ldf');
GO
*/

/* fixed disk , free size */
EXEC master..xp_fixeddrives

/*list all tables space usage*/
WITH cte AS
(
SELECT t.NAME AS TableName
	  ,s.Name AS SchemaName
	  ,p.rows AS RowCounts
	  ,SUM(a.total_pages) * 8 / 1024 AS TotalSpaceMB
	  ,SUM(a.used_pages) * 8 / 1024 AS UsedSpaceMB
	  ,(SUM(a.total_pages) - SUM(a.used_pages)) * 8 / 1024 AS UnusedSpaceMB
  FROM sys.tables t INNER JOIN sys.indexes i ON t.OBJECT_ID = i.object_id
		INNER JOIN sys.partitions p ON i.object_id = p.OBJECT_ID AND i.index_id = p.index_id
		INNER JOIN sys.allocation_units a ON p.partition_id = a.container_id
		LEFT OUTER JOIN sys.schemas s ON t.schema_id = s.schema_id
  WHERE t.NAME NOT LIKE 'dt%'
	AND t.is_ms_shipped = 0
	AND i.OBJECT_ID > 255
  GROUP BY t.Name
		  ,s.Name
		  ,p.Rows
)
SELECT * FROM cte ORDER by UsedSpaceMB DESC


/*list all databases space usage*/
WITH cte AS 
(
  SELECT database_id ,type ,size * 8.0 / 1024 size FROM sys.master_files
)
SELECT name
      ,(SELECT SUM(size) FROM cte WHERE type = 0 AND cte.database_id = db.database_id)DataFileSizeMB
      ,(SELECT SUM(size) FROM cte WHERE type = 1 AND cte.database_id = db.database_id)LogFileSizeMB
FROM sys.databases db
