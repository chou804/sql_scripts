dbcc ind('TEST1', 'dbo.DataRows', -1)

SELECT page_level AS Level
  , allocated_page_file_id AS PageFID
  , allocated_page_page_id AS PagePID
  , previous_page_file_id AS PrevPageFID
  , previous_page_page_id AS PrevPagPID
  , next_page_file_id AS NextPageFID
  , next_page_page_id NextPagePID
  , page_type_desc AS PageType
  , *
  FROM sys.dm_db_database_page_allocations 
	( 
		DB_ID()						--@DatabaseId
	  , OBJECT_ID('dbo.DataRows' )	--@TableId
	  , NULL						--@IndexId
	  , NULL						--@PartionID
	  , 'DETAILED'					--@Mode
	  )
  ORDER BY page_level DESC, previous_page_page_id;

--root index level => logical read 1
DBCC PAGE (IndexInternals, 1, 234 , 3);
GO

--intermediate index level => logical read 1
DBCC PAGE (IndexInternals, 1, 235 , 3);
GO

--leaf page => logical read 3
DBCC TRACEON (3604);
GO
DBCC PAGE (IndexInternals, 1, 1616 , 3);
GO