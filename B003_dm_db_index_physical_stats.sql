SELECT OBJECT_NAME(i.object_id)AS table_name
	 , i.name AS index_name
	 , dips.index_id
	 , dips.index_type_desc
	 , dips.index_depth
	 , dips.index_level
	 , dips.record_count
	 , dips.page_count
	 , dips.forwarded_record_count
	 , dips.avg_page_space_used_in_percent
	 , dips.min_record_size_in_bytes
	 , dips.max_record_size_in_bytes
	 , dips.avg_record_size_in_bytes
	 , dips.fragment_count
	 , dips.avg_fragment_size_in_pages
	 , dips.avg_fragmentation_in_percent
	 , dips.alloc_unit_type_desc
  FROM sys.dm_db_index_physical_stats(DB_ID(), OBJECT_ID('dbo.ProductContent'), NULL, NULL, 'DETAILED')dips
	   JOIN sys.indexes AS i
		   ON dips.index_id=i.index_id
		  AND i.object_id=dips.object_id
  WHERE i.name = 'IX_ProductContent_id_SalesMix_inc'