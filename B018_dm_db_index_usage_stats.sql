SELECT i.index_id
	 , i.name AS [Index]
	 , ius.user_seeks AS Seeks
	 , ius.user_scans AS Scans
	 , ius.user_lookups AS Lookups
	 , ius.user_seeks+ius.user_scans+ius.user_lookups AS Reads
	 , ius.user_updates AS Updates
	 , convert(nvarchar(MAX), ius.last_user_seek, 21) AS [Last_Seek]
	 , convert(nvarchar(MAX), ius.last_user_scan, 21) AS [Last_Scan]
	 , convert(nvarchar(MAX), ius.last_user_lookup, 21) AS [Last_Lookup]
	 , convert(nvarchar(MAX), ius.last_user_update, 21) AS [Last_Update]
  FROM sys.tables t
	   JOIN sys.indexes i
		   ON t.object_id=i.object_id
	   JOIN sys.schemas s
		   ON t.schema_id=s.schema_id
	   LEFT OUTER JOIN sys.dm_db_index_usage_stats ius
		   ON ius.database_id=DB_ID()
		  AND ius.object_id=i.object_id
		  AND ius.index_id=i.index_id
  WHERE i.object_id=OBJECT_ID('dbo.FrontendProductCategory_SalesMix')
  ORDER BY i.index_id;


SELECT i.name AS index_name
	 , i.type_desc
	 , c.name AS column_name
	 , ic.is_included_column AS Included
	 , c.is_identity AS [Identity]
	 , i.is_primary_key AS [Primary]
	 , i.is_unique AS [Unique]
	 , i.is_unique_constraint AS Unique_C
	 , c.is_computed AS Computed
	 , c.is_nullable AS Nullable
	 , ic.is_descending_key AS [Desc]
	 , t.name AS type_name
	 , CAST(CASE
			WHEN t.name IN(N'nchar', N'nvarchar')
			 AND c.max_length<>-1 THEN c.max_length/2
				ELSE c.max_length
			END AS int)AS max_length
	 , t.precision
	 , t.scale
	 , t.is_user_defined
	 , t.is_table_type
  FROM sys.indexes AS i
	   INNER JOIN sys.index_columns AS ic
		   ON ic.column_id>0
		  AND ic.index_id=CAST(i.index_id AS int)
		  AND ic.object_id=i.object_id
	   INNER JOIN sys.columns AS c
		   ON c.object_id=ic.object_id
		  AND c.column_id=ic.column_id
	   INNER JOIN sys.types AS t
		   ON c.user_type_id=t.user_type_id
  WHERE i.object_id=OBJECT_ID('dbo.FrontendProductCategory_SalesMix')
  ORDER BY i.index_id, ic.index_column_id;