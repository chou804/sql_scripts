select t.name, i.rows
from sys.tables t
	inner join sys.sysindexes as i
	on t.object_id = i.id and i.indid < 2
	order by i.rows desc