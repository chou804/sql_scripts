/*============================================================================
  File:     LogSetup.sql

  Summary:  Create some nasty log waits

  Date:     March 2011

------------------------------------------------------------------------------
  Written by Paul S. Randal, SQLskills.com

  (c) 2011, SQLskills.com. All rights reserved.

  For more scripts and sample code, check out 
    http://www.SQLskills.com

  You may alter this code for your own *non-commercial* purposes. You may
  republish altered code as long as you include this copyright and give due
  credit, but you must obtain prior permission before blogging this code.
  
  THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF 
  ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED 
  TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
  PARTICULAR PURPOSE.
============================================================================*/

-- Let's create a slow drive.. using MCM USB stick

/*
USE master;
GO
DROP DATABASE SlowLogFile;
GO
*/

CREATE DATABASE SlowLogFile ON PRIMARY (
    NAME = 'SlowLogFile_data',
    FILENAME = N'D:\SQLskills\SlowLogFile_data.mdf')
LOG ON (
    NAME = 'SlowLogFile_log',
    FILENAME = N'G:\SlowLogFile_log.ldf',
    --FILENAME = N'C:\SQLskills\SlowLogFile_log.ldf',
    SIZE = 5MB,
    FILEGROWTH = 1MB);
GO

ALTER DATABASE SlowLogFile SET RECOVERY SIMPLE;
GO

USE SlowLogFile;
GO

CREATE TABLE BadKeyTable (
	c1 UNIQUEIDENTIFIER DEFAULT NEWID () ROWGUIDCOL,
    c2 DATETIME DEFAULT GETDATE (),
	c3 CHAR (400) DEFAULT 'a');
CREATE CLUSTERED INDEX BadKeyTable_CL ON
	BadKeyTable (c1);
CREATE NONCLUSTERED INDEX BadKeyTable_NCL ON
	BadKeyTable (c2);
GO


-- Fire up 200 clients and watch waits...

-- Stop test
-- Then replace G: with C:
-- Fire up 200 clients and look at stats, latches

