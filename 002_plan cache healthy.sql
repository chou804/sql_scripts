/*
 http://www.sqlskills.com/blogs/kimberly/plan-cache-and-optimizing-for-adhoc-workloads/
*/
SELECT objtype AS CacheType
	, COUNT_BIG(*)AS [Total Plans]
	, SUM(CAST(size_in_bytes AS decimal(18, 2)))/1024/1024 AS [Total MBs]
	, AVG(usecounts)AS [Avg Use Count]
	, SUM(CAST(CASE
				WHEN usecounts=1 THEN size_in_bytes
					ELSE 0
				END AS decimal(18, 2)))/1024/1024 AS [Total MBs - USE Count 1]
	, SUM(CASE
		  WHEN usecounts=1 THEN 1
			  ELSE 0
		  END)AS [Total Plans - USE Count 1]
  FROM sys.dm_exec_cached_plans
  GROUP BY objtype
  ORDER BY [Total MBs - USE Count 1] DESC;


--Listing 1: Query to Find Single-Use Plans
SELECT text
	, cp.objtype
	, cp.size_in_bytes
  FROM sys.dm_exec_cached_plans AS cp CROSS APPLY sys.dm_exec_sql_text(cp.plan_handle)st
  WHERE cp.cacheobjtype=N'Compiled Plan'
	AND cp.objtype IN(N'Adhoc', N'Prepared')
	AND cp.usecounts=1
  ORDER BY cp.size_in_bytes DESC
  OPTION(RECOMPILE);


--Listing 2: Script to Find Plans with Missing Indexes
WITH XMLNAMESPACES(DEFAULT N'http://schemas.microsoft.com/sqlserver/2004/07/showplan')
	SELECT dec.usecounts
		, dec.refcounts
		, dec.objtype
		, dec.cacheobjtype
		, des.dbid
		, des.text
		, deq.query_plan
	 FROM sys.dm_exec_cached_plans AS dec CROSS APPLY sys.dm_exec_sql_text(dec.plan_handle)AS des CROSS APPLY sys.dm_exec_query_plan(dec.plan_handle)AS deq
	 WHERE deq.query_plan.exist(N'/ShowPlanXML/BatchSequence/Batch/Statements/StmtSimple/QueryPlan/MissingIndexes/MissingIndexGroup')<>0
	 ORDER BY dec.usecounts DESC;


--Listing 3: Script to Find Plans with Implicit Conversion Warnings
WITH XMLNAMESPACES(DEFAULT N'http://schemas.microsoft.com/sqlserver/2004/07/showplan')
	SELECT cp.query_hash
		, cp.query_plan_hash
		, ConvertIssue=operators.value('@ConvertIssue', 'nvarchar(250)')
		, Expression=operators.value('@Expression', 'nvarchar(250)')
		, qp.query_plan
	 FROM sys.dm_exec_query_stats cp CROSS APPLY sys.dm_exec_query_plan(cp.plan_handle)qp CROSS APPLY query_plan.nodes('//Warnings/PlanAffectingConvert')rel(operators);


--Listing 4: Script to Find Plans with Key Lookup and Clustered Index Seek Operators
WITH XMLNAMESPACES(DEFAULT N'http://schemas.microsoft.com/sqlserver/2004/07/showplan')
	SELECT cp.query_hash
		, cp.query_plan_hash
		, PhysicalOperator=operators.value('@PhysicalOp', 'nvarchar(50)')
		, LogicalOp=operators.value('@LogicalOp', 'nvarchar(50)')
		, AvgRowSize=operators.value('@AvgRowSize', 'nvarchar(50)')
		, EstimateCPU=operators.value('@EstimateCPU', 'nvarchar(50)')
		, EstimateIO=operators.value('@EstimateIO', 'nvarchar(50)')
		, EstimateRebinds=operators.value('@EstimateRebinds', 'nvarchar(50)')
		, EstimateRewinds=operators.value('@EstimateRewinds', 'nvarchar(50)')
		, EstimateRows=operators.value('@EstimateRows', 'nvarchar(50)')
		, Parallel=operators.value('@Parallel', 'nvarchar(50)')
		, NodeId=operators.value('@NodeId', 'nvarchar(50)')
		, EstimatedTotalSubtreeCost=operators.value('@EstimatedTotalSubtreeCost', 'nvarchar(50)')
	 FROM sys.dm_exec_query_stats cp CROSS APPLY sys.dm_exec_query_plan(cp.plan_handle)qp CROSS APPLY query_plan.nodes('//RelOp')rel(operators);


--Listing 6: Code to Group by the Query Hashes
SELECT COUNT(*)AS Count
	, query_stats.query_hash
	, query_stats.statement_text AS Text
  FROM(
	  SELECT QS.*
			, SUBSTRING(ST.text, QS.statement_start_offset/2+1, (CASE statement_end_offset
																WHEN-1 THEN DATALENGTH(ST.text)
																	ELSE QS.statement_end_offset
																END-QS.statement_start_offset)/2+1)AS statement_text
		FROM sys.dm_exec_query_stats AS QS CROSS APPLY sys.dm_exec_sql_text(QS.sql_handle)AS ST)AS query_stats
  GROUP BY query_stats.query_hash
		, query_stats.statement_text
  ORDER BY 1 DESC;
