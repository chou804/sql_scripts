SELECT  r.session_id AS spid
			 ,r.blocking_session_id AS blk_spid
			 ,r.command
			 ,SUBSTRING(qt.text,r.statement_start_offset / 2 + 1,(CASE r.statement_end_offset
																  WHEN-1 THEN DATALENGTH(qt.text)
																	  ELSE r.statement_end_offset
																  END - r.statement_start_offset) / 2 + 1)AS SQL
			 ,r.cpu_time
			 ,r.logical_reads AS [logi_reads]
			 ,r.total_elapsed_time [total_times]
			 ,r.status
			 ,r.wait_time
			 ,r.wait_type
			 ,r.last_wait_type
			 ,r.start_time
			 ,d.name AS dbname
			 ,s.host_name
			 ,s.login_name
			 ,s.program_name
			 ,c.client_net_address
			 --,cp.size_in_bytes
			 --,cp.cacheobjtype
			 --,cp.objtype
  FROM sys.dm_exec_requests r WITH (nolock) 
		--LEFT JOIN sys.dm_exec_cached_plans cp on r.plan_handle = cp.plan_handle
		LEFT JOIN sys.dm_exec_sessions s ON r.session_id = s.session_id
		LEFT JOIN sys.dm_exec_connections c WITH (nolock)ON r.session_id = c.session_id
		LEFT JOIN sys.databases d ON r.database_id = d.database_id
		CROSS APPLY sys.dm_exec_sql_text(r.sql_handle)qt
  WHERE r.session_id <> @@SPID
  ORDER BY cpu_time DESC
  OPTION(RECOMPILE)