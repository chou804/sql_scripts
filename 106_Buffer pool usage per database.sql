SELECT database_id AS [DB ID]
	  ,DB_NAME(database_id)AS [DB Name]
	  ,CONVERT(decimal(11,3),COUNT(*) * 8 / 1024.0)AS [Buffer Pool Size (MB)]
  FROM sys.dm_os_buffer_descriptors WITH (nolock)
  GROUP BY database_id
  ORDER BY [Buffer Pool Size (MB)] DESC
  OPTION(RECOMPILE);

