WHILE (1 = 1)
  BEGIN
    BEGIN TRANSACTION

    --update sql here
    UPDATE TOP ( 1000 ) SalesMix
    SET deliveryType=1
	  WHERE deliveryType is null

    IF @@ROWCOUNT = 0
      BEGIN
        COMMIT TRANSACTION
         BREAK
      END
    COMMIT TRANSACTION
    -- 1 second delay
    WAITFOR DELAY '00:00:01'
  END 
GO

