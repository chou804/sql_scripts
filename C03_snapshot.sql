
-- check isolation snapshot level enabled
SELECT
   name 'database',
   s.snapshot_isolation_state,
   snapshot_isolation_state_desc,
   is_read_committed_snapshot_on 
FROM
   sys.databases s


-- enable isolation snapshot level
ALTER DATABASE SET ALLOW SNAPSHOT ISOLATION ON


SET TRANSACTION ISOLATION LEVEL SNAPSHOT

