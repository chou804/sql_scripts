select c.session_id,qt.text as [SQL], c.connect_time, c.last_read, c.last_write, c.client_net_address
,s.login_name,s.login_time, s.program_name, s.status, s.cpu_time, s.total_scheduled_time, s.total_elapsed_time, s.last_request_start_time, s.last_request_end_time
from sys.dm_exec_connections c JOIN sys.dm_exec_sessions s on c.session_id = s.session_id
	cross apply sys.dm_exec_sql_text(c.most_recent_sql_handle) qt
where c.net_transport = 'TCP'
order by c.client_net_address, c.session_id

select c.client_net_address, count(*)
from sys.dm_exec_connections c
where c.net_transport = 'TCP'
group by c.client_net_address

select host_name, host_process_id, count(*)
from sys.dm_exec_sessions 
where is_user_process=1 and host_name='U-BE01017'
group by host_name, host_process_id
order by host_name


SELECT ts.*
	  ,qt.text AS SQL ,c.connect_time ,c.last_read ,c.last_write ,c.client_net_address
	  ,s.login_name ,s.login_time ,s.program_name ,s.status ,s.cpu_time
	  ,s.total_scheduled_time ,s.total_elapsed_time ,s.last_request_start_time ,s.last_request_end_time
  FROM sys.dm_tran_session_transactions ts JOIN sys.dm_exec_connections c ON ts.session_id = c.session_id
		JOIN sys.dm_exec_sessions s ON c.session_id = s.session_id
		OUTER APPLY sys.dm_exec_sql_text(c.most_recent_sql_handle)qt
  ORDER BY c.client_net_address,c.session_id