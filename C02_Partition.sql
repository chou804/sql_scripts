-- 查看 partition schema, function 使用的 table

select s.name As SchemaName, t.name As TableName
From sys.tables t
    Inner Join sys.schemas s On t.schema_id = s.schema_id
    Inner Join sys.partitions p on p.object_id = t.object_id
Where p.index_id In (0, 1)
Group By s.name, t.name
Having Count(*) > 1
Order By s.name, t.name;



select ps.Name PartitionScheme, pf.name PartitionFunction
 from sys.indexes i
 join sys.partition_schemes ps on ps.data_space_id = i.data_space_id
 join sys.partition_functions pf on pf.function_id = ps.function_id 
where i.object_id = object_id('Orders')