
--50 most expensive queries
SELECT TOP 50 SUBSTRING(qt.text,qs.statement_start_offset / 2 + 1,(CASE qs.statement_end_offset
																   WHEN-1 THEN DATALENGTH(qt.text)
																	   ELSE qs.statement_end_offset
																   END - qs.statement_start_offset) / 2 + 1)AS Sql
			 ,qs.execution_count AS [Exec Cnt]
			 ,cp.objtype
			 ,(qs.total_logical_reads + qs.total_logical_writes) / qs.execution_count AS [Avg IO]
			 ,qs.total_logical_reads AS [Total Reads]
			 ,qs.last_logical_reads AS [Last Reads]
			 ,qs.total_logical_writes AS [Total Writes]
			 ,qs.last_logical_writes AS [Last Writes]
			 ,qs.total_worker_time AS [Total Worker Time]
			 ,qs.last_worker_time AS [Last Worker Time]
			 ,qs.total_elapsed_time / 1000 AS [Total Elps Time]
			 ,qs.last_elapsed_time / 1000 AS [Last Elps Time]
			 ,qs.creation_time AS [Compile Time]
			 ,qs.last_execution_time AS [Last Exec Time]
			 ,cp.size_in_bytes
			 ,cp.cacheobjtype
			--,qp.query_plan AS [Plan]
  FROM sys.dm_exec_query_stats qs WITH (nolock) 
	JOIN sys.dm_exec_cached_plans cp on qs.plan_handle = cp.plan_handle
	CROSS APPLY sys.dm_exec_sql_text(qs.sql_handle)qt 
	CROSS APPLY sys.dm_exec_query_plan(qs.plan_handle)qp
  ORDER BY [Avg IO] DESC
  OPTION(RECOMPILE)



--50 most expensive stored procedure
SELECT TOP 50 s.name + '.' + p.name AS [Procedure]
			 ,(ps.total_logical_reads + ps.total_logical_writes) / ps.execution_count AS [Avg IO]
			 ,ps.execution_count AS [Exec Cnt]
			 ,ps.cached_time AS Cached
			 ,ps.last_execution_time AS [Last Exec Time]
			 ,ps.total_logical_reads AS [Total Reads]
			 ,ps.last_logical_reads AS [Last Reads]
			 ,ps.total_logical_writes AS [Total Writes]
			 ,ps.last_logical_writes AS [Last Writes]
			 ,ps.total_worker_time AS [Total Worker Time]
			 ,ps.last_worker_time AS [Last Worker Time]
			 ,ps.total_elapsed_time AS [Total Elapsed Time]
			 ,ps.last_elapsed_time AS [Last Elapsed Time]
			 ,cp.size_in_bytes
			 --,qp.query_plan AS [Plan]
  FROM sys.procedures AS p WITH (nolock) JOIN sys.schemas s WITH (nolock)ON p.schema_id = s.schema_id
		JOIN sys.dm_exec_procedure_stats AS ps WITH (nolock) ON p.object_id = ps.object_id 
		LEFT JOIN sys.dm_exec_cached_plans cp on ps.plan_handle = cp.plan_handle
		OUTER APPLY sys.dm_exec_query_plan(ps.plan_handle)qp
  ORDER BY [Avg IO] DESC
  OPTION(RECOMPILE);
