;
WITH Waits
  AS (SELECT wait_type
        ,wait_time_ms
        ,waiting_tasks_count
        ,100. * wait_time_ms / SUM(wait_time_ms)OVER()AS Pct
        ,ROW_NUMBER()OVER(ORDER BY wait_time_ms DESC)AS RowNum
      FROM sys.dm_os_wait_stats WITH (nolock)
      WHERE wait_type NOT IN 
      /* Filtering out non-essential system waits */
      (N'CLR_SEMAPHORE',N'LAZYWRITER_SLEEP',N'RESOURCE_QUEUE',N'SLEEP_TASK',N'SLEEP_SYSTEMTASK',N'SQLTRACE_BUFFER_FLUSH',
      N'WAITFOR',N'LOGMGR_QUEUE',N'CHECKPOINT_QUEUE',N'REQUEST_FOR_DEADLOCK_SEARCH',N'XE_TIMER_EVENT',N'BROKER_TO_FLUSH',
      N'BROKER_TASK_STOP',N'CLR_MANUAL_EVENT',N'CLR_AUTO_EVENT',N'DISPATCHER_QUEUE_SEMAPHORE',N'FT_IFTS_SCHEDULER_IDLE_WAIT',
      N'XE_DISPATCHER_WAIT',N'XE_DISPATCHER_JOIN',N'SQLTRACE_INCREMENTAL_FLUSH_SLEEP',N'ONDEMAND_TASK_QUEUE',N'BROKER_EVENTHANDLER',
      N'SLEEP_BPOOL_FLUSH',N'SLEEP_DBSTARTUP',N'DIRTY_PAGE_POLL',N'BROKER_RECEIVE_WAITFOR',N'HADR_FILESTREAM_IOMGR_IOCOMPLETION',
      N'WAIT_XTP_CKPT_CLOSE',N'SP_SERVER_DIAGNOSTICS_SLEEP',N'BROKER_TRANSMITTER',N'QDS_PERSIST_TASK_MAIN_LOOP_SLEEP','MSQL_XP',
      N'QDS_CLEANUP_STALE_QUERIES_TASK_MAIN_LOOP_SLEEP',N'WAIT_XTP_HOST_WAIT',N'WAIT_XTP_OFFLINE_CKPT_NEW_LOG',
      N'HADR_WORK_QUEUE',N'HADR_NOTIFICATION_DEQUEUE',N'HADR_CLUSAPI_CALL',N'HADR_TIMER_TASK',N'PREEMPTIVE_SP_SERVER_DIAGNOSTICS',N'HADR_LOGCAPTURE_WAIT',N'HADR_SYNC_COMMIT',N'REDO_THREAD_PENDING_WORK' --屬於AG, cluster之間溝通的
    ))
  SELECT w1.wait_type AS [Wait Type]
      ,w1.waiting_tasks_count AS [Wait Count]
      ,CONVERT(decimal(12,3),w1.wait_time_ms / 1000.0)AS [Wait Time]
      ,CONVERT(decimal(12,1),w1.wait_time_ms / w1.waiting_tasks_count)AS [Avg Wait Time]
      ,CONVERT(decimal(6,3),w1.Pct)AS [Percent]
      ,CONVERT(decimal(6,3),SUM(w2.Pct))AS [Running Percent]
    FROM Waits w1 JOIN Waits w2 ON w2.RowNum <= w1.RowNum
    GROUP BY w1.RowNum
        ,w1.wait_type
        ,w1.wait_time_ms
        ,w1.waiting_tasks_count
        ,w1.Pct
    HAVING SUM(w2.Pct) - w1.pct < 95
    OPTION(RECOMPILE)

/*
PAGEIOLATCH_* wait types occur when SQL Server is waiting for an I/O subsystem to bring a data page from disk to the buffer pool.
IO_COMPLETION often indicates slow tempdb I/O performance during sort and hash operators
BACKUPIO is a sign of slow performance of a backup disk drive, and it often occurs with an ASYNC_IO_COMPLETION wait type
WRITELOG and LOGBUFFER waits are a sign of bad transaction log I/O throughput.
*/