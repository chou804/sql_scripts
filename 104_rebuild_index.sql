ALTER INDEX idxFrontendProductCategory_SalesMix_fatherId_Inc 
	ON dbo.FrontendProductCategory_SalesMix 
	REBUILD WITH (SORT_IN_TEMPDB=ON, STATISTICS_NORECOMPUTE=ON);