SELECT l.resource_type
	  ,d.name [database]
	  ,associated_entity = CASE WHEN l.resource_type = 'OBJECT' THEN object_name(resource_associated_entity_id, DB_ID(d.name))
							ELSE CONVERT(nvarchar(20),resource_associated_entity_id) END
	  ,l.request_mode
	  ,r.session_id AS spid
	  ,r.blocking_session_id AS blk_spid
	  ,r.command
	  ,SUBSTRING(qt.text,r.statement_start_offset / 2 + 1,(CASE r.statement_end_offset
														   WHEN-1 THEN DATALENGTH(qt.text)
															   ELSE r.statement_end_offset
														   END - r.statement_start_offset) / 2 + 1)AS SQL
	  ,r.cpu_time
	  ,r.logical_reads AS logi_reads
	  ,r.total_elapsed_time total_times
	  ,r.status
	  ,r.wait_time
	  ,r.wait_type
	  ,r.last_wait_type
	  ,r.start_time
	  ,s.host_name
	  ,s.login_name
	  ,s.program_name
	  ,c.client_net_address
  FROM sys.dm_tran_locks l WITH (nolock) JOIN sys.databases d WITH (nolock)ON l.resource_database_id = d.database_id
		LEFT OUTER JOIN sys.dm_exec_sessions s WITH (nolock)ON l.request_session_id = s.session_id
		LEFT OUTER JOIN sys.dm_exec_requests r WITH (nolock)ON l.request_session_id = r.session_id
		LEFT OUTER JOIN sys.objects o WITH (nolock)ON l.resource_associated_entity_id = o.object_id
		LEFT OUTER JOIN sys.dm_exec_connections c WITH (nolock)ON l.request_session_id = c.session_id
		OUTER APPLY sys.dm_exec_sql_text(r.sql_handle)qt
  WHERE resource_associated_entity_id <> 0 and l.request_session_id <> @@SPID
