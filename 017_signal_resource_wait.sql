/*
Signal waits indicate the waiting times for the CPU (should not exceed 25 percent)
Resource waits indicate the waiting times for resources, such as for pages from disk.
*/
SELECT SUM (signal_wait_time_ms) AS [Signal Wait Time (ms)]
               , CONVERT (decimal( 7, 4 ), 100.0*SUM( signal_wait_time_ms)/SUM (wait_time_ms)) AS [% Signal waits]
               , SUM (wait_time_ms- signal_wait_time_ms)AS [Resource Wait Time (ms)]
               , CONVERT (decimal( 7, 4 ), 100.0*SUM( wait_time_ms-signal_wait_time_ms )/SUM( wait_time_ms))AS [% Resource waits]
   FROM sys.dm_os_wait_stats WITH ( nolock)
   OPTION( RECOMPILE);