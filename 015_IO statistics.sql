SELECT fs.database_id AS [DB ID]
    ,fs.file_id AS [File Id]
    ,mf.name AS [File Name]
    ,mf.physical_name AS [File Path]
    ,mf.type_desc AS Type
    ,fs.sample_ms AS [Time]
    ,fs.num_of_reads AS Reads
    ,fs.num_of_bytes_read AS [Read Bytes]
    ,fs.num_of_writes AS Writes
    ,fs.num_of_bytes_written AS [Written Bytes]
    ,fs.num_of_reads + fs.num_of_writes AS [IO Count]
    ,CONVERT(decimal(5,2),100.0 * fs.num_of_bytes_read / (fs.num_of_bytes_read + fs.num_of_bytes_written))AS [Read %]
    ,CONVERT(decimal(5,2),100.0 * fs.num_of_bytes_written / (fs.num_of_bytes_read + fs.num_of_bytes_written))AS [Write %]
    ,fs.io_stall_read_ms AS [Read Stall]
    ,fs.io_stall_write_ms AS [Write Stall]
    ,CASE WHEN fs.num_of_reads = 0 THEN 0.000
        ELSE CONVERT(decimal(12,3),1.0 * fs.io_stall_read_ms / fs.num_of_reads)
     END AS [Avg Read Stall]
    ,CASE WHEN fs.num_of_writes = 0 THEN 0.000
        ELSE CONVERT(decimal(12,3),1.0 * fs.io_stall_write_ms / fs.num_of_writes)
     END AS [Avg Write Stall]
  FROM sys.dm_io_virtual_file_stats(NULL,NULL)fs 
    JOIN sys.master_files mf WITH (nolock) ON fs.database_id = mf.database_id AND fs.file_id = mf.file_id
    JOIN sys.databases d WITH (nolock) ON d.database_id = fs.database_id
  WHERE fs.num_of_reads + fs.num_of_writes > 0
  OPTION(RECOMPILE)