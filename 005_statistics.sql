/**/
dbcc show_statistics('PhraseLibrary' ,_WA_Sys_0000000C_025D5595)

/*Returning all statistics properties for a table*/
SELECT sp.stats_id ,name ,filter_definition ,last_updated ,rows
	  ,rows_sampled ,steps ,unfiltered_rows ,modification_counter
  FROM sys.stats AS stat CROSS APPLY sys.dm_db_stats_properties(stat.object_id,stat.stats_id)AS sp
  WHERE stat.object_id = OBJECT_ID('Books');

/*Returning statistics properties for frequently modified objects*/
SELECT 
      obj.name, obj.object_id, stat.name, stat.stats_id, last_updated, modification_counter
  FROM sys.objects AS obj 
  JOIN sys.stats stat ON stat.object_id = obj.object_id
  CROSS APPLY sys.dm_db_stats_properties(stat.object_id, stat.stats_id) AS sp
  WHERE modification_counter > 1000;


