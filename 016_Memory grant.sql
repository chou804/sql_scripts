SELECT mg.session_id
               , t.text AS SQL
               , qp.query_plan AS [Plan]
               , mg.is_small
               , mg.dop
               , mg.query_cost
               , mg.request_time
               , mg.required_memory_kb
               , mg.requested_memory_kb
               , mg.wait_time_ms
               , mg.grant_time
               , mg.granted_memory_kb
               , mg.used_memory_kb
               , mg.max_used_memory_kb
   FROM sys.dm_exec_query_memory_grants mg WITH (nolock )
                 CROSS APPLY sys. dm_exec_sql_text(mg.sql_handle) t
                 CROSS APPLY sys. dm_exec_query_plan(mg.plan_handle) AS qp
   OPTION( RECOMPILE);