/*
The primary filegroup has one data file stored on the M: drive. 
The second filegroup, Entities, hasone data file stored on the N: drive. 
The last filegroup, Orders, has two data files stored on the O: and P: drives. 
Finally, there is a transaction log file stored on the L: drive.

tip:
1. Avoid using the PRIMARY filegroup for anything but system objects
2. Transaction log does not benefit from multiple files
3. It is recommended that all files in a single filegroup have the same initial size and auto-growth parameters with
   grow size being defined in megabytes rather than by percent.
4. Every time SQL Server grows the files, it fills the newly allocated space with zeros. This process blocks all sessions
*/
CREATE DATABASE OrderEntryDb 
	ON PRIMARY(NAME = N'OrderEntryDb',FILENAME = N'm:\OEDb.mdf'),
	FILEGROUP Entities(NAME = N'OrderEntry_Entities_F1',FILENAME = N'n:\OEEntities_F1.ndf'),
	FILEGROUP Orders(NAME = N'OrderEntry_Orders_F1',FILENAME = N'o:\OEOrders_F1.ndf'),
				    (NAME = N'OrderEntry_Orders_F2',FILENAME = N'p:\OEOrders_F2.ndf')
	log ON(NAME = N'OrderEntryDb_log',FILENAME = N'l:\OrderEntryDb_log.ldf')


/*
add filegroup and new file add to it
*/
ALTER DATABASE OrderEntryDb ADD FILEGROUP dataFG
GO
ALTER DATABASE OrderEntryDb 
ADD FILE
(
	NAME = N'OrderEntry_Data_F1',
	FILENAME = N'D:\SQLData\Data_F1.ndf',
	SIZE = 5MB, --initial size
	FILEGROWTH = 5MB
) TO FILEGROUP dataFG
GO


name                        fileid filename                            filegroup size     maxsize       growth   usage
--------------------------- ------ ----------------------------------- --------- -------- ------------- -------- ---------
OrderEntryDb                1      D:\SQLData\OEDb.mdf                 PRIMARY   4160 KB  Unlimited     1024 KB  data only
OrderEntryDb_log            2      D:\SQLData\OrderEntryDb_log.ldf     NULL      1024 KB  2147483648 KB 10%      log only
OrderEntry_Entities_F1      3      D:\SQLData\OEEntities_F1.ndf        Entities  1024 KB  Unlimited     1024 KB  data only
OrderEntry_Orders_F1        4      D:\SQLData\OEOrders_F1.ndf          Orders    1024 KB  Unlimited     1024 KB  data only
OrderEntry_Orders_F2        5      D:\SQLData\OEOrders_F2.ndf          Orders    1024 KB  Unlimited     1024 KB  data only
OrderEntry_Data_F1          6      D:\SQLData\Data_F1.ndf              dataFG    5120 KB  Unlimited     5120 KB  data only
