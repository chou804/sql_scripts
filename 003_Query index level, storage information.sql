--DBCC SHOW_statistics('dbo.SalesMix', idx_id_SaleStatus_Inc)


DECLARE
   @tblName AS nvarchar(50) = 'dbo.SalesMix';

SELECT OBJECT_NAME(i.object_id)AS table_name
    ,i.name AS index_name
    ,dips.index_id
    ,dips.index_type_desc
    ,dips.index_depth
    ,dips.index_level
    ,dips.record_count
    ,dips.forwarded_record_count
    ,dips.page_count
    ,dips.avg_page_space_used_in_percent
    ,dips.fragment_count
    ,dips.avg_fragmentation_in_percent
    ,dips.min_record_size_in_bytes
    ,dips.max_record_size_in_bytes
    ,dips.avg_record_size_in_bytes
    ,dips.alloc_unit_type_desc
  FROM sys.dm_db_index_physical_stats(DB_ID(),OBJECT_ID(@tblName),NULL,NULL,'DETAILED')dips 
    JOIN sys.indexes AS i ON dips.index_id = i.index_id AND i.object_id = dips.object_id;

SELECT i.name AS index_name
    ,i.type_desc
    ,c.name AS column_name
    ,ic.key_ordinal
    ,ic.is_included_column as [Included]
    ,c.is_identity as [Identity]
    ,i.is_primary_key as [Primary]
    ,i.is_unique as [Unique]
    ,i.is_unique_constraint as [Unique_C]
    ,c.is_computed as [Computed]
    ,c.is_nullable as [Nullable]
    ,ic.is_descending_key as [Desc]
    ,t.name AS type_name
    ,t.max_length
    ,t.precision
    ,t.scale
    ,t.is_user_defined
    ,t.is_table_type
  FROM sys.indexes AS i INNER JOIN sys.index_columns AS ic ON ic.column_id > 0
                              AND ic.index_id = CAST(i.index_id AS int)
                              AND ic.object_id = i.object_id
    INNER JOIN sys.columns AS c ON c.object_id = ic.object_id
                   AND c.column_id = ic.column_id
    INNER JOIN sys.types AS t ON c.user_type_id = t.user_type_id
  WHERE i.object_id = OBJECT_ID(@tblName)
  ORDER BY type_desc, index_name, Included, key_ordinal

 
SELECT so.name AS [Table]
        , c.name AS [Column]
        , t.name AS Type
        , CAST (CASE
                      WHEN t.name IN(N'nchar' , N'nvarchar' )
                      AND c.max_length <>- 1 THEN c. max_length / 2
                            ELSE c.max_length
                      END AS int) AS Length
        , CAST (c. precision AS int) AS Precision
        , CAST (c. scale AS int) AS Scale
        , c.is_nullable
        , c.is_identity
  FROM sysobjects AS so
          INNER JOIN sys. columns AS c
                 ON c.object_id = so. id
          LEFT OUTER JOIN sys.types AS t
                 ON t.user_type_id = c. user_type_id
                AND t.system_type_id = c. system_type_id
  WHERE so.id = OBJECT_ID(@tblName )
  ORDER BY c.column_id ASC;
 
 