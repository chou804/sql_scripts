SELECT o.name
	  ,dips.index_type_desc
	  ,dips.avg_fragmentation_in_percent
	  ,dips.fragment_count
	  ,dips.avg_fragment_size_in_pages
	  ,dips.page_count
	  ,dips.avg_page_space_used_in_percent
	  ,dips.record_count
	  ,dips.forwarded_record_count
  FROM sys.dm_db_index_physical_stats(DB_ID(),NULL,NULL,NULL,'DETAILED')dips JOIN sys.objects o ON dips.object_id = o.object_id
  WHERE forwarded_record_count > 0