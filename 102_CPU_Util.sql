/*
Column name  Data type Description 
cpu_ticks  bigint Current CPU tick count. CPU ticks are obtained from the processor's RDTSC counter. It is a monotonically increasing number. 
ms_ticks   bigint Number of milliseconds since the computer was started. 

**Ref: http://connect.microsoft.com/SQLServer/feedback/ViewFeedback.aspx?FeedbackID=292638
*/
    declare @ts_now bigint, @avgcpu tinyint
    --select @ts_now = cpu_ticks / convert(float, cpu_ticks_in_ms) from sys.dm_os_sys_info  --SQL Server 2008 remove cpu_ticks_in_ms 
    select @ts_now = ms_ticks from sys.dm_os_sys_info  --SQL Server 2008 remove cpu_ticks_in_ms, ms_ticks = cpu_ticks / convert(float, cpu_ticks_in_ms) from sys.dm_os_sys_info 

    --SELECT @ts_now  
    select record_id,
        dateadd(ms, -1 * (@ts_now - [timestamp]), GetDate()) as EventTime, 
        SQLProcessUtilization,
        SystemIdle,
        100 - SystemIdle - SQLProcessUtilization as OtherProcessUtilization
    from (
        select 
            record.value('(./Record/@id)[1]', 'int') as record_id,
            record.value('(./Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') as SystemIdle,
            record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') as SQLProcessUtilization,
            timestamp
        from (
            select timestamp, convert(xml, record) as record 
            from sys.dm_os_ring_buffers 
            where ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR'
            and record like '%<SystemHealth>%') as x
        ) as y 
    order by record_id desc;
